# SkyMavis Challenge

A project price tracking application for crypto assets.

## Getting started

- This project just configure for only Android.
- Please make sure you run npm install before run android application.

## Deploy

Link apk application: [https://drive.google.com/file/d/1ABAWjgaLMOT5nncw4toWeIiwdWsLjVGp/view?usp=sharing](https://drive.google.com/file/d/1ABAWjgaLMOT5nncw4toWeIiwdWsLjVGp/view?usp=sharing)

---
