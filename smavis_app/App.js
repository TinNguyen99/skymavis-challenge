import React, {createContext, useEffect, useState} from 'react';
import ApplicationNavigator from './src/navigators';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {ChangeLaguage} from './src/utils/change_language';

export const ThemeContext = createContext();
const App = () => {
  const [themeStyle, setThemeStyle] = useState('dark');
  const [language, setLanguage] = useState('en');

  const toggleTheme = () => {
    setThemeStyle(themeStyle === 'dark' ? 'light' : 'dark');
  };
  const toggleLanguage = () => {
    setLanguage(language === 'en' ? 'vi' : 'en');
  };
  useEffect(() => {
    const getData = async () => {
      try {
        const valueKeyTheme = await AsyncStorage.getItem('keyTheme');
        const valueKeyLan = await AsyncStorage.getItem('languageStore');
        const dataTheme = JSON.parse(valueKeyTheme);
        const dataLanguage = JSON.parse(valueKeyLan);
        if (dataTheme != null) {
          setThemeStyle(dataTheme);
        }
        if (dataLanguage != null) {
          setLanguage(dataLanguage);
          ChangeLaguage(dataLanguage);
        }
      } catch (e) {
        console.log('App > getData: ', e);
      }
    };
    getData();
  }, [language, themeStyle]);

  const valueTheme = {
    themeStyle,
    language,
    toggleTheme,
    toggleLanguage,
  };

  return (
    <ThemeContext.Provider value={valueTheme}>
      <ApplicationNavigator />
    </ThemeContext.Provider>
  );
};

export default App;
