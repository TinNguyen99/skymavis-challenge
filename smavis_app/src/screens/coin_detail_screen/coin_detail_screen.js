import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useContext, useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {StackActions} from '@react-navigation/native';
import {getMarketCoin} from '../../services';
import {darkStyles, lightStyles} from './styles';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import {ThemeContext} from '../../../App';
import COLORS from '../../theme/colors';
import {convertStringToCurrencyDecimal} from '../../utils/convert_price';
import {i18} from '../../../utils/i18next';
import {useTranslation} from 'react-i18next';
import {Loading} from '../../components/indicator_loading';
import {CATCH_IMAGE} from '../../config';

const CoinDetailScreen = ({route, navigation}) => {
  const id = route.params?.id;
  const {t} = useTranslation();

  const [itemCoin, setItemCoin] = useState({});
  const [isLoading, setLoading] = useState(false);
  const [isAdding, setIsAdding] = useState(false);

  useEffect(() => {
    setLoading(true);
    const getIsAdding = async () => {
      var tempStorage = await AsyncStorage.getItem('watchList');
      if (tempStorage === null) {
        setIsAdding(false);
      } else {
        const data = JSON.parse(tempStorage);
        if (
          data.some(value => {
            return value.id === id;
          })
        ) {
          setIsAdding(true);
        } else {
          setIsAdding(false);
        }
      }
    };
    getIsAdding();
    let mounted = true;
    getMarketCoin(id).then(items => {
      if (mounted) {
        setItemCoin(items);
        setLoading(false);
      }
    });
    setLoading(false);
    return () => (mounted = false);
  }, []);

  const onAddWatchlist = async () => {
    setIsAdding(true);
    var listStoraged = await AsyncStorage.getItem('watchList');
    const arrayParse = JSON.parse(listStoraged) || [];
    arrayParse.push(itemCoin);
    await AsyncStorage.setItem('watchList', JSON.stringify(arrayParse));
  };

  const onDeleteWatchlist = async () => {
    setIsAdding(false);
    var listStoraged = await AsyncStorage.getItem('watchList');
    const data = JSON.parse(listStoraged);
    const indexItem = data.findIndex(e => {
      return e.id === id;
    });
    if (indexItem > -1) {
      data.splice(indexItem, 1);
    }
    await AsyncStorage.setItem('watchList', JSON.stringify(data));
  };

  const theme = useContext(ThemeContext);

  const market_cap_rank =
    itemCoin.market_cap_rank === undefined ? '' : itemCoin.market_cap_rank;
  const current_price =
    itemCoin?.market_data?.current_price?.usd === undefined
      ? ''
      : convertStringToCurrencyDecimal(
          itemCoin?.market_data?.current_price?.usd,
        );

  const price_change_percentage_24h =
    itemCoin?.market_data?.price_change_percentage_24h === undefined
      ? ''
      : itemCoin?.market_data?.price_change_percentage_24h.toFixed(2);

  const high_24h =
    itemCoin.market_data?.high_24h?.usd === undefined
      ? ''
      : convertStringToCurrencyDecimal(
          itemCoin.market_data?.high_24h?.usd || 0,
        );
  const low_24h =
    itemCoin.market_data?.low_24h?.usd === undefined
      ? ''
      : convertStringToCurrencyDecimal(itemCoin.market_data?.low_24h?.usd || 0);
  return (
    <View
      style={
        theme.themeStyle === 'dark'
          ? darkStyles.container
          : lightStyles.container
      }>
      {isLoading ? (
        Loading()
      ) : (
        <View>
          <View style={darkStyles.header}>
            <TouchableOpacity
              onPress={() => {
                const popAction = StackActions.pop(1);
                navigation.dispatch(popAction);
              }}>
              <View>
                <Icon
                  name="arrow-back-outline"
                  size={30}
                  color={COLORS.lightGrey}
                  style={darkStyles.backArrow}
                />
              </View>
            </TouchableOpacity>
            <Text
              style={
                theme.themeStyle === 'dark'
                  ? darkStyles.title
                  : lightStyles.title
              }>
              {itemCoin.name}
            </Text>
            <TouchableOpacity
              onPress={isAdding ? onDeleteWatchlist : onAddWatchlist}>
              <Icon
                name="heart"
                size={25}
                color={isAdding ? COLORS.lightGreen : COLORS.lightGrey}
                style={darkStyles.heartIcon}
              />
            </TouchableOpacity>
          </View>
          <View>
            <Image
              style={darkStyles.logo}
              source={{
                uri: itemCoin?.image?.large ?? CATCH_IMAGE,
              }}
            />
            <Text
              style={
                theme.themeStyle === 'dark'
                  ? darkStyles.symbol
                  : lightStyles.symbol
              }>
              {itemCoin.symbol?.toUpperCase()}
            </Text>
            <View style={darkStyles.groupInfo}>
              <Text
                style={
                  theme.themeStyle === 'dark'
                    ? darkStyles.infoTitle
                    : lightStyles.infoTitle
                }>
                {t('Rank')}
              </Text>
              <Text
                style={
                  theme.themeStyle === 'dark'
                    ? darkStyles.infoBody
                    : lightStyles.infoBody
                }>
                {market_cap_rank}
              </Text>
            </View>

            <View style={darkStyles.groupInfo}>
              <Text
                style={
                  theme.themeStyle === 'dark'
                    ? darkStyles.infoTitle
                    : lightStyles.infoTitle
                }>
                {t('Current_price')}
              </Text>
              <Text
                style={
                  theme.themeStyle === 'dark'
                    ? darkStyles.infoBody
                    : lightStyles.infoBody
                }>
                {current_price}
              </Text>
            </View>

            <View style={darkStyles.groupInfo}>
              <Text
                style={
                  theme.themeStyle === 'dark'
                    ? darkStyles.infoTitle
                    : lightStyles.infoTitle
                }>
                {t('Price_change_per_24h')}
              </Text>
              <Text
                style={
                  theme.themeStyle === 'dark'
                    ? darkStyles.infoBody
                    : lightStyles.infoBody
                }>
                {price_change_percentage_24h}%
              </Text>
            </View>

            <View style={darkStyles.groupInfo}>
              <Text
                style={
                  theme.themeStyle === 'dark'
                    ? darkStyles.infoTitle
                    : lightStyles.infoTitle
                }>
                {t('High_24h')}
              </Text>
              <Text
                style={
                  theme.themeStyle === 'dark'
                    ? darkStyles.infoBody
                    : lightStyles.infoBody
                }>
                {high_24h}
              </Text>
            </View>

            <View style={darkStyles.groupInfo}>
              <Text
                style={
                  theme.themeStyle === 'dark'
                    ? darkStyles.infoTitle
                    : lightStyles.infoTitle
                }>
                {t('Low_24h')}
              </Text>
              <Text
                style={
                  theme.themeStyle === 'dark'
                    ? darkStyles.infoBody
                    : lightStyles.infoBody
                }>
                {low_24h}
              </Text>
            </View>
          </View>
        </View>
      )}
    </View>
  );
};

export default CoinDetailScreen;
