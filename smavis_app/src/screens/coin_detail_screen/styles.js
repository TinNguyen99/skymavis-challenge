import {StyleSheet} from 'react-native';
import COLORS from '../../theme/colors';
import {responsiveWidth, responsiveHeight} from '../../utils/metrics';

const darkStyles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: COLORS.darkBackground,
  },
  header: {
    height: 50,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 20,
    color: COLORS.white,
    fontWeight: 'bold',
  },
  backArrow: {
    marginStart: 20,
  },
  heartIcon: {
    marginEnd: 20,
  },
  logo: {
    width: 150,
    height: 150,
    borderRadius: 50,
    alignSelf: 'center',
    marginVertical: responsiveHeight(10),
  },
  symbol: {
    fontSize: 18,
    color: COLORS.lightGrey,
    fontWeight: 'bold',
    alignSelf: 'center',
    textAlign: 'center',
    marginVertical: responsiveHeight(10),
    width: '90%',
    borderBottomWidth: 1,
    borderBottomColor: COLORS.lightGrey,
    paddingVertical: 10,
  },
  groupInfo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: responsiveHeight(10),
    paddingHorizontal: responsiveWidth(20),
  },
  infoTitle: {
    color: COLORS.white,
    paddingVertical: responsiveHeight(10),
    fontSize: 18,
    fontWeight: 'bold',
  },
  infoBody: {
    color: COLORS.lightGrey,
    fontSize: 16,
    fontWeight: 'bold',
  },
});

const lightStyles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: COLORS.lightBackground,
  },
  title: {
    fontSize: 20,
    color: COLORS.softBlack,
    fontWeight: 'bold',
  },
  symbol: {
    fontSize: 18,
    color: COLORS.softBlack,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginVertical: responsiveHeight(10),
  },
  infoTitle: {
    color: COLORS.softBlack,
    fontSize: 18,
    fontWeight: 'bold',
    paddingVertical: responsiveHeight(10),
  },
  infoBody: {
    color: COLORS.softBlack,
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export {darkStyles, lightStyles};
