import {StyleSheet} from 'react-native';
import {responsiveWidth, responsiveHeight} from '../../../utils/metrics';
import COLORS from '../../../theme/colors';
const darkStyles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: COLORS.darkBackground,
  },
  itemCoin: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 20,
    marginHorizontal: responsiveWidth(20),
    marginBottom: responsiveHeight(20),
    backgroundColor: COLORS.hightGrey,
  },
  logo: {
    width: 35,
    height: 35,
    borderRadius: 50,
    marginHorizontal: responsiveWidth(20),
    marginVertical: responsiveHeight(20),
  },
  groupName: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  coinName: {
    fontSize: 17,
    color: COLORS.white,
    fontWeight: 'bold',
  },
  coinSymbol: {
    fontSize: 15,
    color: COLORS.lightGrey,
    fontWeight: 'bold',
  },
  groupPrice: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginEnd: responsiveWidth(20),
  },
  increase: {
    color: COLORS.darkGreen,
    fontWeight: '400',
    fontSize: 15,
    marginStart: responsiveWidth(20),
  },
  decrease: {
    color: COLORS.darkRed,
    fontWeight: '400',
    fontSize: 15,
    marginStart: responsiveWidth(20),
  },
  coinTrending: {
    flex: 1,
    borderRadius: 20,
    width: responsiveWidth(150),
    height: responsiveHeight(220),
    marginEnd: responsiveWidth(20),
    marginBottom: responsiveHeight(20),
    backgroundColor: COLORS.hightGrey,
  },
  trendingSymbol: {
    fontSize: 15,
    color: COLORS.lightGrey,
    fontWeight: 'bold',
    marginHorizontal: responsiveWidth(20),
  },
  trendingName: {
    fontSize: 17,
    color: COLORS.white,
    fontWeight: 'bold',
    marginHorizontal: responsiveWidth(20),
    marginBottom: responsiveHeight(20),
  },
  groupTrend: {marginHorizontal: 20},
  searchBox: {
    borderRadius: 20,
    color: COLORS.white,
    backgroundColor: COLORS.hightGrey,
    marginHorizontal: responsiveWidth(20),
    marginVertical: responsiveHeight(20),
    paddingHorizontal: 20,
  },
  title: {
    fontSize: 20,
    color: COLORS.white,
    fontWeight: 'bold',
    alignSelf: 'center',
    paddingVertical: responsiveHeight(20),
  },
  notHaveCryto: {
    fontSize: 20,
    color: COLORS.white,
    fontWeight: 'bold',
    alignSelf: 'center',
    textAlign: 'center',
    paddingVertical: responsiveHeight(20),
  },
});

const lightStyles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: COLORS.lightBackground,
  },
  itemCoin: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 20,
    marginHorizontal: responsiveWidth(20),
    marginBottom: responsiveHeight(20),
    backgroundColor: COLORS.mediumGrey,
  },
  coinName: {
    fontSize: 17,
    color: COLORS.softBlack,
    fontWeight: 'bold',
  },
  coinSymbol: {
    fontSize: 15,
    color: COLORS.softBlack,
    fontWeight: 'bold',
  },

  title: {
    fontSize: 20,
    color: COLORS.softBlack,
    fontWeight: 'bold',
    alignSelf: 'center',
    paddingVertical: responsiveHeight(20),
  },
  notHaveCryto: {
    fontSize: 20,
    color: COLORS.softBlack,
    fontWeight: 'bold',
    textAlign: 'center',
    alignSelf: 'center',
    paddingVertical: responsiveHeight(20),
  },
});

export {darkStyles, lightStyles};
