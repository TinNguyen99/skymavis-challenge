import React, {useEffect, useState, useContext} from 'react';
import {View, Text, TouchableOpacity, Image, FlatList} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {ThemeContext} from '../../../../App';
import {darkStyles, lightStyles} from './styles';
import {i18} from '../../../utils/i18next';
import {useTranslation} from 'react-i18next';
import {CATCH_IMAGE} from '../../../config';
import {HeaderWithTitle} from '../../../components/header_page';

const WalletScreen = ({navigation}) => {
  const theme = useContext(ThemeContext);
  const {t} = useTranslation();

  const [items, setItem] = useState([]);

  useEffect(() => {
    const getAsycData = navigation.addListener('focus', async () => {
      const response = await AsyncStorage.getItem('watchList');
      const data = JSON.parse(response);
      setItem(data);
    });
    getAsycData;
    return getAsycData;
  }, [navigation]);

  const ItemCoin = ({item}) => {
    return (
      <TouchableOpacity
        style={
          theme.themeStyle === 'dark'
            ? darkStyles.itemCoin
            : lightStyles.itemCoin
        }
        onPress={() => {
          navigation.navigate('CoinDetail', {id: item.id});
        }}>
        <Image
          style={darkStyles.logo}
          source={{
            uri: item.image?.thumb ?? CATCH_IMAGE,
          }}
        />
        <View style={darkStyles.groupName}>
          <Text
            style={
              theme.themeStyle === 'dark'
                ? darkStyles.coinName
                : lightStyles.coinName
            }>
            {item.name}
          </Text>
          <Text
            style={
              theme.themeStyle === 'dark'
                ? darkStyles.coinSymbol
                : lightStyles.coinSymbol
            }>
            {item.symbol?.toUpperCase()}
          </Text>
        </View>

        <View style={darkStyles.groupPrice}>
          <Text
            style={
              theme.themeStyle === 'dark'
                ? darkStyles.coinName
                : lightStyles.coinName
            }>
            {t('Rank')}: {item.market_cap_rank}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <View
      style={
        theme.themeStyle === 'dark'
          ? darkStyles.container
          : lightStyles.container
      }>
      {HeaderWithTitle(t('Wallet'), theme.themeStyle)}
      {items?.length > 0 ? (
        <FlatList
          data={items}
          keyExtractor={(item, index) => index.toString()}
          renderItem={ItemCoin}
        />
      ) : (
        <Text
          style={
            theme.themeStyle === 'dark'
              ? darkStyles.notHaveCryto
              : lightStyles.notHaveCryto
          }>
          {t('Your_wallet')}
        </Text>
      )}
    </View>
  );
};

export default WalletScreen;
