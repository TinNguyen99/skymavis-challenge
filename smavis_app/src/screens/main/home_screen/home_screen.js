import React, {useEffect, useState, useContext} from 'react';
import {
  View,
  Text,
  Image,
  ActivityIndicator,
  FlatList,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {getMarketList, searchCoins} from '../../../services';
import {fillterTrendingCoin} from '../../../utils/fillter_trending_coin';
import {ThemeContext} from '../../../../App';
import {darkStyles, lightStyles} from './styles';
import {convertStringToCurrencyDecimal} from '../../../utils/convert_price';
import COLORS from '../../../theme/colors';
import {i18} from '../../../utils/i18next';
import {useTranslation} from 'react-i18next';
import {Loading} from '../../../components/indicator_loading';

const HomeScreen = ({navigation}) => {
  const theme = useContext(ThemeContext);
  const {t} = useTranslation();

  const [marketCoinsData, setMarketCoinData] = useState([]);
  const [hotlist, setHotlist] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [isRefreshing, setRefresing] = useState(false);
  const [searchText, setSearchText] = useState('');
  const [searchList, setSearchList] = useState([]);

  useEffect(() => {
    let isMounted = true;
    setLoading(true);
    getMarketList().then(items => {
      if (isMounted) {
        setMarketCoinData(items);
        setLoading(false);
        setHotlist(fillterTrendingCoin(items));
      }
    });

    // Refresh market list every 20s
    const intervalId = setInterval(() => {
      isMounted = true;
      getMarketList().then(items => {
        if (isMounted) {
          setMarketCoinData(items);
        }
      });
    }, 20000);

    return () => {
      clearInterval(intervalId);
      isMounted = false;
    };
  }, []);

  useEffect(() => {
    setSearchText('');
  }, [navigation]);

  const onChangeValue = async item => {
    setLoading(true);
    if (item === '') {
      setSearchList([]);
    } else {
      searchCoins(item).then(items => {
        setLoading(false);
        setSearchList(items.coins);
      });
    }
    setLoading(false);
  };

  useEffect(() => {
    const cleanSearch = navigation.addListener('focus', async () => {
      setSearchText('');
    });
    cleanSearch;
    return cleanSearch;
  }, [navigation]);

  const onRefresh = () => {
    let mounted = true;
    setRefresing(true);
    getMarketList().then(items => {
      if (mounted) {
        setMarketCoinData(items);
        setRefresing(false);
      }
    });
    return () => {
      mounted = false;
    };
  };

  const ItemTrending = ({item}) => {
    return (
      <TouchableOpacity
        style={
          theme.themeStyle === 'dark'
            ? darkStyles.coinTrending
            : lightStyles.coinTrending
        }
        onPress={() => {
          navigation.navigate('CoinDetail', {id: item.id});
        }}>
        <Image
          style={darkStyles.logo}
          source={{
            uri: item.image,
          }}
        />

        <Text
          style={
            theme.themeStyle === 'dark'
              ? darkStyles.trendingSymbol
              : lightStyles.trendingSymbol
          }>
          {item.symbol?.toUpperCase()}
        </Text>
        <Text
          style={
            theme.themeStyle === 'dark'
              ? darkStyles.trendingName
              : lightStyles.trendingName
          }>
          {item.name}
        </Text>
        <Text
          style={
            item.price_change_percentage_24h >= 0
              ? darkStyles.increase
              : darkStyles.decrease
          }>
          {item.price_change_percentage_24h} %
        </Text>
        <Text
          style={
            theme.themeStyle === 'dark'
              ? darkStyles.trendingName
              : lightStyles.trendingName
          }>
          {convertStringToCurrencyDecimal(item.current_price)}
        </Text>
      </TouchableOpacity>
    );
  };

  const ItemCoin = ({item}) => {
    return (
      <TouchableOpacity
        style={
          theme.themeStyle === 'dark'
            ? darkStyles.itemCoin
            : lightStyles.itemCoin
        }
        onPress={() => {
          navigation.navigate('CoinDetail', {id: item.id});
        }}>
        <Image
          style={darkStyles.logo}
          source={{
            uri: item.image,
          }}
        />
        <View style={darkStyles.groupName}>
          <Text
            style={
              theme.themeStyle === 'dark'
                ? darkStyles.coinName
                : lightStyles.coinName
            }>
            {item.name}
          </Text>
          <Text
            style={
              theme.themeStyle === 'dark'
                ? darkStyles.coinSymbol
                : lightStyles.coinSymbol
            }>
            {item.symbol.toUpperCase()}
          </Text>
        </View>

        <View style={darkStyles.groupPrice}>
          <Text
            style={
              theme.themeStyle === 'dark'
                ? darkStyles.coinName
                : lightStyles.coinName
            }>
            {convertStringToCurrencyDecimal(item.current_price)}
          </Text>
          <Text
            style={
              item.price_change_percentage_24h >= 0
                ? darkStyles.increase
                : darkStyles.decrease
            }>
            {item.price_change_percentage_24h} %
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  const ItemSearch = ({item}) => {
    return (
      <TouchableOpacity
        style={
          theme.themeStyle === 'dark'
            ? darkStyles.itemCoin
            : lightStyles.itemCoin
        }
        onPress={() => {
          navigation.navigate('CoinDetail', {id: item.id});
        }}>
        <Image
          style={darkStyles.logo}
          source={{
            uri: item.thumb,
          }}
        />
        <View style={darkStyles.groupName}>
          <Text
            style={
              theme.themeStyle === 'dark'
                ? darkStyles.coinName
                : lightStyles.coinName
            }>
            {item.name}
          </Text>
          <Text
            style={
              theme.themeStyle === 'dark'
                ? darkStyles.coinSymbol
                : lightStyles.coinSymbol
            }>
            {item.symbol.toUpperCase()}
          </Text>
        </View>

        <View style={darkStyles.groupPrice}>
          <Text
            style={
              theme.themeStyle === 'dark'
                ? darkStyles.coinName
                : lightStyles.coinName
            }>
            Rank: {item.market_cap_rank}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <View
      style={
        theme.themeStyle === 'dark'
          ? darkStyles.container
          : lightStyles.container
      }>
      {isLoading ? (
        Loading()
      ) : (
        <View>
          <View>
            <TextInput
              style={
                theme.themeStyle === 'dark'
                  ? darkStyles.searchBox
                  : lightStyles.searchBox
              }
              onChangeText={text => {
                setSearchText(text);
                onChangeValue(text);
              }}
              value={searchText}
              placeholderTextColor={
                theme.themeStyle === 'dark' ? COLORS.lightGrey : 'grey'
              }
              placeholder={t('Search_any_cryto')}
              keyboardType="default"
              maxLength={6}
            />
          </View>
          {searchText === '' ? (
            <View>
              <Text
                style={
                  theme.themeStyle === 'dark'
                    ? darkStyles.titleList
                    : lightStyles.titleList
                }>
                {t('Hot_trending')}
              </Text>
              <View style={darkStyles.groupTrend}>
                <FlatList
                  showsHorizontalScrollIndicator={false}
                  horizontal={true}
                  data={hotlist}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={ItemTrending}
                  onRefresh={onRefresh}
                  refreshing={isRefreshing}
                />
              </View>
              <Text
                style={
                  theme.themeStyle === 'dark'
                    ? darkStyles.titleList
                    : lightStyles.titleList
                }>
                {t('Market_coins_list')}
              </Text>
              <View>
                <FlatList
                  data={marketCoinsData}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={ItemCoin}
                  onRefresh={onRefresh}
                  refreshing={isRefreshing}
                />
              </View>
            </View>
          ) : searchList.length > 0 ? (
            <View
              style={
                theme.themeStyle === 'dark'
                  ? darkStyles.container
                  : lightStyles.container
              }>
              <FlatList
                data={searchList}
                keyExtractor={(item, index) => index.toString()}
                renderItem={ItemSearch}
              />
            </View>
          ) : (
            <View style={{width: '100%', height: '100%', alignItems: 'center'}}>
              <Text
                style={
                  theme.themeStyle === 'dark'
                    ? darkStyles.notHaveCryto
                    : lightStyles.notHaveCryto
                }>
                {t('Not_found')}
              </Text>
            </View>
          )}
        </View>
      )}
    </View>
  );
};

export default HomeScreen;
