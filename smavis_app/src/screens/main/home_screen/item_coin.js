import React from 'react';

import {View, Text, Image, TouchableOpacity} from 'react-native';
import {darkStyles, lightStyles} from './styles';
import {convertStringToCurrencyDecimal} from '../../../utils/convert_price';

export const ItemCoin = ({theme, navigation, item}) => {
  return (
    <TouchableOpacity
      style={
        theme.themeStyle === 'dark' ? darkStyles.itemCoin : lightStyles.itemCoin
      }
      onPress={() => {
        navigation.navigate('CoinDetail', {id: item.id});
      }}>
      <Image
        style={darkStyles.logo}
        source={{
          uri: item.image,
        }}
      />
      <View style={darkStyles.groupName}>
        <Text
          style={
            theme.themeStyle === 'dark'
              ? darkStyles.coinName
              : lightStyles.coinName
          }>
          {item.name}
        </Text>
        <Text
          style={
            theme.themeStyle === 'dark'
              ? darkStyles.coinSymbol
              : lightStyles.coinSymbol
          }>
          {item.symbol}
        </Text>
      </View>

      <View style={darkStyles.groupPrice}>
        <Text
          style={
            theme.themeStyle === 'dark'
              ? darkStyles.coinName
              : lightStyles.coinName
          }>
          {convertStringToCurrencyDecimal(item.current_price)}
        </Text>
        <Text
          style={
            item.price_change_percentage_24h >= 0
              ? darkStyles.increase
              : darkStyles.decrease
          }>
          {item.price_change_percentage_24h}
        </Text>
      </View>
    </TouchableOpacity>
  );
};
