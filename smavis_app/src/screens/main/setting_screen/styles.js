import {StyleSheet} from 'react-native';
import {responsiveWidth, responsiveHeight} from '../../../utils/metrics';
import COLORS from '../../../theme/colors';
const darkStyles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: COLORS.darkBackground,
  },
  groupSetting: {
    flexDirection: 'row',
    borderRadius: 20,
    height: responsiveHeight(70),
    marginHorizontal: responsiveWidth(20),
    paddingEnd: responsiveWidth(10),
    marginBottom: responsiveHeight(20),
    backgroundColor: COLORS.hightGrey,
    justifyContent: 'space-between',
  },
  optionTitle: {
    fontSize: 18,
    color: COLORS.white,
    fontWeight: 'bold',
    paddingStart: responsiveWidth(20),
    paddingVertical: responsiveHeight(20),
  },
  title: {
    fontSize: 22,
    color: COLORS.white,
    fontWeight: 'bold',
    alignSelf: 'center',
    paddingVertical: responsiveHeight(20),
  },
});

const lightStyles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: COLORS.lightBackground,
  },
  optionTitle: {
    fontSize: 18,
    color: COLORS.softBlack,
    fontWeight: 'bold',
    paddingStart: responsiveWidth(20),
    paddingVertical: responsiveHeight(20),
  },
  groupSetting: {
    flexDirection: 'row',
    borderRadius: 20,
    height: responsiveHeight(70),
    marginHorizontal: responsiveWidth(20),
    paddingEnd: responsiveWidth(10),
    marginBottom: responsiveHeight(20),
    backgroundColor: COLORS.mediumGrey,
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 22,
    color: COLORS.softBlack,
    fontWeight: 'bold',
    alignSelf: 'center',
    paddingVertical: responsiveHeight(20),
  },
});

export {darkStyles, lightStyles};
