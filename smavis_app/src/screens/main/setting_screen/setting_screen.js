import React, {useState, useContext} from 'react';
import {View, Text, Switch} from 'react-native';
import {ChangeLaguage} from '../../../utils/change_language';
import {i18} from '../../../utils/i18next';
import {useTranslation} from 'react-i18next';
import {ThemeContext} from '../../../../App';
import {darkStyles, lightStyles} from './styles';
import AsyncStorage from '@react-native-async-storage/async-storage';
import COLORS from '../../../theme/colors';
import {HeaderWithTitle} from '../../../components/header_page';

const SettingScreen = () => {
  const {t} = useTranslation();
  const theme = useContext(ThemeContext);
  const [isEnabledDarkTheme, setIsEnabledDarkTheme] = useState(
    theme.themeStyle === 'dark' ? true : false,
  );
  const [isEnabledLanguage, setIsEnabledLanguage] = useState(
    theme.language === 'en' ? false : true,
  );

  const toggleSwitchLanguage = async () => {
    try {
      theme.toggleLanguage();
      let jsonValue;
      if (theme.language === 'en') {
        jsonValue = JSON.stringify('vi');
        ChangeLaguage('vi');
      } else {
        jsonValue = JSON.stringify('en');
        ChangeLaguage('en');
      }
      await AsyncStorage.setItem('languageStore', jsonValue);
    } catch (e) {
      console.log('Setting > toggleSwitchLanguage: ', e);
    }
    setIsEnabledLanguage(theme.language === 'en' ? false : true);
  };

  const toggleSwitchDarkTheme = async () => {
    theme.toggleTheme();
    try {
      let jsonValue;
      if (theme.themeStyle === 'dark') {
        jsonValue = JSON.stringify('light');
      } else {
        jsonValue = JSON.stringify('dark');
      }
      await AsyncStorage.setItem('keyTheme', jsonValue);
    } catch (e) {
      console.log('Setting > toggleSwitchDarkTheme: ', e);
    }
    setIsEnabledDarkTheme(previousState => !previousState);
  };

  return (
    <View
      style={
        theme.themeStyle === 'dark'
          ? darkStyles.container
          : lightStyles.container
      }>
      {HeaderWithTitle(t('Setting'), theme.themeStyle)}
      <View
        style={
          theme.themeStyle === 'dark'
            ? darkStyles.groupSetting
            : lightStyles.groupSetting
        }>
        <Text
          style={
            theme.themeStyle === 'dark'
              ? darkStyles.optionTitle
              : lightStyles.optionTitle
          }>
          {t('Language')} (en/vn)
        </Text>

        <Switch
          trackColor={{false: COLORS.white, true: COLORS.white}}
          thumbColor={isEnabledLanguage ? COLORS.lightGreen : COLORS.lightGreen}
          onValueChange={toggleSwitchLanguage}
          value={isEnabledLanguage}
        />
      </View>

      <View
        style={
          theme.themeStyle === 'dark'
            ? darkStyles.groupSetting
            : lightStyles.groupSetting
        }>
        <Text
          style={
            theme.themeStyle === 'dark'
              ? darkStyles.optionTitle
              : lightStyles.optionTitle
          }>
          {t('Dark_mode')}
        </Text>

        <Switch
          trackColor={{false: COLORS.white, true: COLORS.white}}
          thumbColor={
            isEnabledDarkTheme ? COLORS.lightGreen : COLORS.lightGreen
          }
          onValueChange={toggleSwitchDarkTheme}
          value={isEnabledDarkTheme}
        />
      </View>
    </View>
  );
};

export default SettingScreen;
