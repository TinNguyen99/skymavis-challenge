export function convertStringToCurrencyDecimal(str) {
  return `$${str?.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}`;
}
