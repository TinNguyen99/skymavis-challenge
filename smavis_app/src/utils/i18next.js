import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import {NativeModules} from 'react-native';
import en from '../translates/en.json';
import vi from '../translates/vi.json';

let defaultLanguage = i18n.language;
defaultLanguage = NativeModules.I18nManager.localeIdentifier.slice(0, 2);
const resources = {
  en,
  vi,
};

i18n.use(initReactI18next).init({
  resources,
  lng: defaultLanguage,
  react: {useSuspense: false},
});

export default i18n;
