export const fillterTrendingCoin = coinList => {
  const coinListCopy = [...coinList];
  const temp = coinListCopy.sort((a, b) => {
    return a.total_volume < b.total_volume;
  });
  const trendingList = temp.slice(0, 9);
  return trendingList;
};
