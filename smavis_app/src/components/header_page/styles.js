import {StyleSheet} from 'react-native';
import {responsiveWidth, responsiveHeight} from '../../utils/metrics';
import COLORS from '../../theme/colors';
const darkStyles = StyleSheet.create({
  title: {
    fontSize: 20,
    color: COLORS.white,
    fontWeight: 'bold',
    alignSelf: 'center',
    paddingVertical: responsiveHeight(20),
  },
});

const lightStyles = StyleSheet.create({
  title: {
    fontSize: 20,
    color: COLORS.softBlack,
    fontWeight: 'bold',
    alignSelf: 'center',
    paddingVertical: responsiveHeight(20),
  },
});

export {darkStyles, lightStyles};
