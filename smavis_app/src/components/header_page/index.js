import React from 'react';
import {Text} from 'react-native';
import {darkStyles, lightStyles} from './styles';

export const HeaderWithTitle = (title, theme) => {
  return (
    <Text style={theme === 'dark' ? darkStyles.title : lightStyles.title}>
      {title}
    </Text>
  );
};
