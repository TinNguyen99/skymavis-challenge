import React from 'react';
import {ActivityIndicator} from 'react-native';

export const Loading = () => {
  return (
    <ActivityIndicator
      style={{justifyContent: 'center', alignItems: 'center'}}
    />
  );
};
