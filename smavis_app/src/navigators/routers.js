import React, {useContext, useEffect, useState} from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import HomeScreen from '../screens/main/home_screen/home_screen';
import WalletScreen from '../screens/main/wallet_screen/wallet_screen';
import SettingScreen from '../screens/main/setting_screen/setting_screen';
import COLORS from '../theme/colors';
import {i18} from '../../../utils/i18next';
import {useTranslation} from 'react-i18next';
import {ThemeContext} from '../../App';

const BottomTab = createBottomTabNavigator();

const Routers = () => {
  const {t} = useTranslation();
  const themes = useContext(ThemeContext);
  const [stateTheme, setStateTheme] = useState(themes.themeStyle);

  useEffect(() => {
    setStateTheme(themes.themeStyle);
  }, [themes]);

  return (
    <BottomTab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          backgroundColor:
            stateTheme === 'dark' ? COLORS.darkTab : COLORS.lightTab,
        },
      }}>
      <BottomTab.Screen
        name={t('Home')}
        component={HomeScreen}
        options={{
          showLabel: false,
          tabBarShowLabel: false,
          tabBarInactiveTintColor: 'green',
          tabBarIconStyle: {display: 'flex'},
          tabBarLabelPosition: 'below-icon',
          tabBarIcon: ({focused}) =>
            focused ? (
              <Icon name="home" size={20} color={COLORS.lightGreen} />
            ) : (
              <Icon name="home-outline" size={20} color={COLORS.lightGreen} />
            ),
        }}
      />
      <BottomTab.Screen
        name={t('Wallet')}
        component={WalletScreen}
        options={{
          showLabel: false,
          tabBarShowLabel: false,
          tabBarIconStyle: {display: 'flex'},
          tabBarLabelPosition: 'below-icon',
          tabBarIcon: ({focused}) =>
            focused ? (
              <Icon name="wallet" size={20} color={COLORS.lightGreen} />
            ) : (
              <Icon name="wallet-outline" size={20} color={COLORS.lightGreen} />
            ),
        }}
      />
      <BottomTab.Screen
        name={t('Setting')}
        component={SettingScreen}
        options={{
          showLabel: false,
          tabBarShowLabel: false,
          tabBarIconStyle: {display: 'flex'},
          tabBarLabelPosition: 'below-icon',
          tabBarIcon: ({focused}) =>
            focused ? (
              <Icon name="settings" size={20} color={COLORS.lightGreen} />
            ) : (
              <Icon
                name="settings-outline"
                size={20}
                color={COLORS.lightGreen}
              />
            ),
        }}
      />
    </BottomTab.Navigator>
  );
};

export default Routers;
