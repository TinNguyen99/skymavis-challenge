import React from 'react';
import Routers from './routers';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import CoinDetailScreen from '../screens/coin_detail_screen/coin_detail_screen';

const Stack = createStackNavigator();

const ApplicationNavigator = () => (
  <NavigationContainer>
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen
        name="Main"
        component={Routers}
        options={{
          animationEnabled: false,
        }}
      />
      <Stack.Screen name="CoinDetail" component={CoinDetailScreen} />
    </Stack.Navigator>
  </NavigationContainer>
);

export default ApplicationNavigator;
