export default {
  hightGrey: '#16202f',
  darkBackground: '#0a1424',
  lightBackground: '#e3e8ee',
  lightGreen: '#35b0a3',
  white: '#eaeaec',
  lightGrey: '#9599a1',
  mediumGrey: '#abb1bc',
  darkGreen: '#4e9248',
  softBlack: '#2d384b',
  darkRed: '#cd2222',
  darkTab: '#09111e',
  lightTab: '#bec5cf',
};
